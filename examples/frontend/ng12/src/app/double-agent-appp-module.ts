import { NgModule, APP_INITIALIZER } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { DoubleAgentValidatorV2Module, DoubleAgentValidator } from '@double-agent-validator/core';
import { environment } from '../environments/environment';

@NgModule({
  imports: [
    ReactiveFormsModule,
    DoubleAgentValidatorV2Module.forRoot({
      ajv: (self as any)['ajv'],
      validationFn: (self as any)['validate']
    }),
  ],
  exports: [DoubleAgentValidatorV2Module],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFactory,
      deps: [DoubleAgentValidator],
      multi: true
    }
  ]
})
export class DoubleAgentAppModule {
  constructor() {
  }
}

// Factory para ser utilizada na inicialização da aplicação, para condicionalmente ler o bundle de arquivo
// local (em produção) ou ler via http quando for em desenvolvimento
export function appInitializerFactory(doubleAgentValidator: any) {
  // função de inicialização para ser executada pelo Angular
  return function () {
    const promiseDoubleAgent = new Promise((resolve, reject) => {
      const scriptAjvLoad = document.createElement('script');
      // chama a função que retorna a url do bundle
      scriptAjvLoad.src = environment.jsonSchemasLocalService;
      scriptAjvLoad.onload = () => {
        // após a carga do bundle (onload), então está pronto para inicializar a aplicação
        doubleAgentValidator.config['ajv'] = (window as any)['ajv'];
        doubleAgentValidator.config['validationFn'] = (window as any)['validate'];
        // resolve a promise e, então, a aplicação Angular inicializará
        resolve(null);
      };
      scriptAjvLoad.onerror = (error: any) => {
        const errorMsg = 'Failed to load doubleAgentValidator' + error.message;
        console.error(errorMsg);
        // falha a inicialização do Angular, caso haja erro no bundle
        reject(errorMsg);
      };
      // adiciona o element script apontando para o bundle no body do document,
      // para que o javascript do bundle seja processado
      document.body.appendChild(scriptAjvLoad);
    });

    return Promise.all([promiseDoubleAgent]);
  };
}
