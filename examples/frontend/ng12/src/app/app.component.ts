import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DoubleAgentFormGroupBuilder } from '@double-agent-validator/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'ng12';

  formContribuinte!: FormGroup;
  private contribuinte: any = null;

  constructor(private formGroupBuilder: DoubleAgentFormGroupBuilder) {}

  ngOnInit() {
    // <any> here is needed because of small diferences between angular 2 versions in the AbstractControl definition
    this.formContribuinte = <any>this.formGroupBuilder.build('contribuinte-v1');
  }

  submterContribuinte() {
    console.log(this.formContribuinte.value);
    this.contribuinte = this.formContribuinte.value;
  }
}
