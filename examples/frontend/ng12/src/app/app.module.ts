import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DoubleAgentAppModule } from './double-agent-appp-module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, ReactiveFormsModule, DoubleAgentAppModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
