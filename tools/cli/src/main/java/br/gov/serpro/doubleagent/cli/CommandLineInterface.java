package br.gov.serpro.doubleagent.cli;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.script.ScriptException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.gson.Gson;

import br.gov.serpro.doubleagent.BundledJsonSchemaValidator;
import br.gov.serpro.doubleagent.model.ValidationResult;

/**
 * This is a command line interface (CLI) to be used to validate data
 * 
 * @author abner
 *
 */
@SuppressWarnings("restriction")
public class CommandLineInterface {

	@Parameter(names = { "--bundle",
			"-b" }, description = "Pass a file Path or Url poiting to a ajv bundle", required = true)
	String bundleUrlOrPath;

	@Parameter(names = "--data", description = "Data to be validated")
	String data;

	@Parameter(names = "--schema", description = "Schema which the data will be validated against")
	String schemaName;

	@Parameter(names = { "--help", "-h" }, help = true)
	private boolean help;

	@Parameter(names = { "--server", "-s" })
	boolean server;

	@Parameter(names = { "--port", "-p" })
	Integer port;

	@Parameter(names = { "--icp", "-i" })
	Boolean interProcessComunication;

	private BundledJsonSchemaValidator validator;

	
	public CommandLineInterface() {
	}

	public static void main(String[] argv) throws ScriptException, IOException {
		CommandLineInterface cli = new CommandLineInterface();
		JCommander _commander = JCommander.newBuilder().addObject(cli).build();
		_commander.parse(argv);
		if (cli.help) {
			System.out.println("Double Agent Validator Cli - by Abner Oliveira");
			_commander.usage();
		} else {
			if (cli.server) {
				try {
					cli.setupValidator();
				} catch (MalformedURLException e) {
					throw new RuntimeException("Failed to setup the validator. Wrong file path or url", e);
				} 
				new HttpServer(cli.validator, 4242);
			} else {				
				cli.run();
			}
		}
	}

	public void run() {
		System.out.println("CLI BUNDLE: " + this.bundleUrlOrPath);

		// configure the validator
		try {
			setupValidator();
		} catch (MalformedURLException e) {
			throw new RuntimeException("Failed to setup the validator. Wrong file path or url", e);
		} catch (ScriptException e) {
			throw new RuntimeException("Failed to parse the bundle passed. Are you sure it is a valid javascript?", e);
		} catch (IOException e) {
			throw new RuntimeException("Could not read the file or url passed.", e);
		}

		// validate the data passed
		try {
			validateData();
		} catch (Exception e) {
			throw new RuntimeException("Invalid input data!", e);
		}

		try {
			runValidation();
		} catch (Exception e) {
			throw new RuntimeException("Failed to validate!", e);
		}
	}

	private void setupValidator() throws MalformedURLException, ScriptException, IOException {
		// initializes the double agent validator, loading the bundle
		this.validator = new BundledJsonSchemaValidator(new URL(this.bundleUrlOrPath));
	}

	private void validateData() throws Exception {
		// check if the data passed is a valid JSON
		Gson gson = new Gson();
		Object o = gson.fromJson(this.data, Object.class);

	}

	private void runValidation() throws Exception {
		// executes the validation]
		ValidationResult result = this.validator.validate(this.schemaName, this.data);
		System.out.println(result.toJSON());
	}
}
