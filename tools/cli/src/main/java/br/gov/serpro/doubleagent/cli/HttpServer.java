package br.gov.serpro.doubleagent.cli;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptException;

import br.gov.serpro.doubleagent.BundledJsonSchemaValidator;
import br.gov.serpro.doubleagent.model.ValidationResult;
import fi.iki.elonen.NanoHTTPD;

@SuppressWarnings("restriction")
public class HttpServer extends NanoHTTPD {

	private BundledJsonSchemaValidator jsonSchemaValidator;

	public static final String SERVER_MSG = "\nRunning Double Agent Validator Tiny Server! Now you can post params schema and data to http://localhost:%d  \n";
	
	public static void main(String[] args) throws ScriptException, IOException {
		BundledJsonSchemaValidator validator = new BundledJsonSchemaValidator(HttpServer.class.getResource("/bundle.js"));
		new HttpServer(validator, 8080);
	}

	public HttpServer(BundledJsonSchemaValidator jsonSchemaValidator, int port) throws IOException {
		super(port);
		this.jsonSchemaValidator = jsonSchemaValidator;
		start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
		System.out.println(String.format(SERVER_MSG, port));
	}

	@Override
	public Response serve(IHTTPSession session) {
		if (session.getMethod() == Method.GET) {
			return processGET(session);
		}
		if (session.getMethod() == Method.POST) {
			try {
				return processPOST(session);
			} catch (Exception e) {
				return newFixedLengthResponse(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "SERVER INTERNAL ERROR: " + e.getMessage());
			}
		}
		return null;
	}

	private Response processGET(IHTTPSession session) {
		String msg = "<html><body><h1>Hello server</h1>\n";
		Map<String, String> parms = session.getParms();

		msg += "<form action='?' method='post' application/x-www-form-urlencoded>\n  " 
		+ "<p>Schema name: <input type='text' name='schema'></p>\n"
		+ "<p>Data [in JSON]: <textarea name='data'></textarea></p>\n"
		+ "<p><button type='submit'>Validate</button></p>"
				+ "</form>\n";

		return newFixedLengthResponse(msg + "</body></html>\n");

	}

	private Response processPOST(IHTTPSession session) throws ResponseException, IOException {
		final HashMap<String, String> filesParms = new HashMap<String, String>();
        session.parseBody(filesParms);

		if ((session.getParms().get("schema") == null) && (session.getParms().get("data") == null)) {
			throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Missing params: schema and data");
		}

		if (session.getParms().get("schema") == null) {
			throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Missing schema param");
		}

		if (session.getParms().get("data") == null) {
			throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Missing data param");
		}
		
		
		ValidationResult result;
		try {
			result = this.jsonSchemaValidator.validate(session.getParms().get("schema"), session.getParms().get("data"));
		} catch (Exception e) {
			throw new ResponseException(Response.Status.INTERNAL_ERROR, "Failed to validate: ", e);
		}
		
		return newFixedLengthResponse(Response.Status.OK, "application/json", result.toJSON());

	}
}
