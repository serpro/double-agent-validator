package br.gov.serpro.doubleagent.model;

// import com.fitbur.testify.Cut;
// import com.fitbur.testify.junit.UnitTest;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 80129498572 on 29/11/16.
 */
@RunWith(JUnit4.class)
public class ValidationResultTest {
    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();


    public ValidationResult validationResult;

    public void setup() {
        validationResult = new ValidationResult();
    }

    public void itInitializesWithHasErrorEqualsFalse() {
        softly.assertThat(validationResult.hasErrors()).isFalse();
    }

    @Test
    public void buildValidationResultWithNonExpectedData(){

        Map<String, Map<String, Object>> resultData = new HashMap<String, Map<String, Object>>() ;

        Map<String, Object> resultItem = new HashMap<String, Object>();

        resultItem.put("bla", null);
        resultData.put("0", resultItem);

        ValidationResult result = ValidationResult.buildValidationResult(resultData);

        softly.assertThat(result.hasErrors()).isTrue();

        softly.assertThat(result.getErrors().size()).isEqualTo(1);
    }
    
    @Test
    public void checkJsonSerialization() {
    	 ValidationResult result = new ValidationResult();
    	 ArrayList<ItemValidationResult> errors =  new ArrayList<ItemValidationResult>();
    	 errors.add(new ItemValidationResult("required", ".nome"));
    	 result.setErrors(errors);
    	 System.out.println(result.toJSON());
    }


}
