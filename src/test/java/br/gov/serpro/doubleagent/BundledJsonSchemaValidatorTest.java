package br.gov.serpro.doubleagent;

import java.io.IOException;
import java.io.InputStream;

import javax.script.ScriptException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.assertj.core.api.Assertions.assertThat;

import br.gov.serpro.doubleagent.model.ValidationResult;

@RunWith(JUnit4.class)
public class BundledJsonSchemaValidatorTest {
    
    private BundledJsonSchemaValidator validator;
    @Before
    public void setup() throws ScriptException, IOException {
        InputStream is = this.getClass().getResourceAsStream("/validators/js/bundle.js");
        validator = new BundledJsonSchemaValidator(is);
    }
    
    @Test
    public void validateDataValida() throws Exception {
        ValidationResult result = validator.validate("schema1", "{}");
        assertThat(result).isNotNull();
        assertThat(result.containsErrors("required")).isTrue();
    }

}
