package br.gov.serpro.doubleagent;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;

import br.gov.serpro.doubleagent.model.ValidationResult;

public class BundledJsonSchemaValidator {
    
    private ScriptEngine nashorn = (ScriptEngine) new ScriptEngineManager().getEngineByName("nashorn");

    private Charset encoding = Charset.forName("UTF-8");
    
    public BundledJsonSchemaValidator(InputStream scriptCode) throws ScriptException, IOException {
        System.out.println("[BundledJsonSchemaValidator] => Constructor Called - " + System.currentTimeMillis());
        this.initializeNashorn(scriptCode);       
    }

    
    public BundledJsonSchemaValidator(URL url) throws ScriptException, IOException {
    	InputStream is = url.openStream();
    	initializeNashorn(is);
    }
    
    private void initializeNashorn(InputStream scriptCode) throws ScriptException, IOException{
    	 /**
         * Polyfill to "console" browser object
         *
         * @see https://github.com/facebook/react/issues/3037
         */
        this.nashorn.eval("var global=this,console={}, window=global;console.debug=print,console.warn=print,console.log=print;");
        
        String script = IOUtils.toString(scriptCode, encoding);
        // execute scripts
        this.nashorn.eval(script);
    }
    
    public synchronized ValidationResult validate(String schemaName, String jsonTarget) throws Exception {
        return this.validate(schemaName, jsonTarget, false);
    }

    @SuppressWarnings("unchecked")
    public synchronized ValidationResult validate(String schemaName, String jsonTarget, boolean removeEmptyStrings)
        throws Exception {

        nashorn.put("schemaName", schemaName);

        // ugly but necessary to the object behaves like a common anonymous
        // javascript Object
        // and so, works as it should in the browser
        nashorn.eval("var value = " + jsonTarget + ";");

        // calls the validation function

        nashorn.eval("if (validate === undefined) validate = window.validate;");
        nashorn.eval("if (validate === undefined) validate = global.validate;");

        nashorn.eval("var result = validate('" + schemaName
            + "', value);");
        
        nashorn.eval("result = result ? { errors: result } : null");

        // get the result from the nashorn environment
        Map<String, Object> jsObjResult = (Map<String, Object>) nashorn.getBindings(ScriptContext.ENGINE_SCOPE)
            .get("result");

        // if the value is true, so get a result indicating "no error"
        if (jsObjResult == null) {
            return new ValidationResult();
        } else {
            // otherwise builds a ValidationResult object containing the errors
            Map<String, Map<String, Object>> resultMap = (Map<String, Map<String, Object>>) jsObjResult.get("errors");
            return ValidationResult.buildValidationResult(resultMap);
        }

    }
    
    public void loadSchema(String schemaName, String schema) throws ScriptException {
        nashorn.eval("ajv.addSchema(" + schema + ", \"" + schemaName + "\");");
    }
    
    public void loadSchema(String schema) throws ScriptException {
        nashorn.eval("ajv.addSchema(" + schema + ");");
    }

}
