package br.gov.serpro.doubleagent.model;

import com.google.gson.Gson;

/**
 * Created by 80129498572 on 24/04/17.
 * http://epoberezkin.github.io/ajv/#options
 */
public class AjvConstructorOptions {
    private boolean coerceTypes = false;
    private boolean allErrors = true;
    private boolean verbose = false;
    private boolean v5 = true;
    private boolean $data = false;

    public static String convertToJson(AjvConstructorOptions obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);
    }


    public AjvConstructorOptions() {

    }

    public static AjvConstructorOptions build() {
        return new AjvConstructorOptions();
    }

    public AjvConstructorOptions coerceTypes(boolean value){
        this.coerceTypes = value;
        return this;
    }

    public boolean getCoerceTypes() {
        return this.coerceTypes;
    }

    public AjvConstructorOptions verbose(boolean value) {
        this.verbose = value;
        return this;
    }

    public boolean getVerbose() {
        return this.verbose;
    }

    public AjvConstructorOptions allErrors(boolean value) {
        this.allErrors = value;
        return this;
    }

    public boolean getAllErrors() {
        return this.allErrors;
    }

    public AjvConstructorOptions v5(boolean value) {
        this.v5 = value;
        return this;
    }

    public boolean getV5() {
        return this.v5;
    }

    public AjvConstructorOptions $data(boolean value) {
        this.$data = value;
        return this;
    }

    public boolean get$data() {
        return this.$data;
    }


    public String asJsonString() {
        return AjvConstructorOptions.convertToJson(this);
    }

}
