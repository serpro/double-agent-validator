package br.gov.serpro.doubleagent.model;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Item do resulvado de validacao
 * Compoe a coleçao @Link ValidationResult.errors
 *
 * @author: abner.oliveira
 */
public class ItemValidationResult implements Serializable {

    private static final long serialVersionUID = 3489999103242660985L;
    public static final String STRING_FORMAT = "%s # %s : %s";
    public static final String STRING_FORMAT_PARAMS = " [%s]=%s";

    @Expose
    private String keyword;
    
    @Expose
    private String dataPath;
    
    @Expose
    private String schemaPath;
    
    @Expose
    private Map<String, String> params;
    
    @Expose
    private String message;
    
    @Expose
    private Map<String, String> data;

    public ItemValidationResult() {}

    public ItemValidationResult(String keyword) {
        this.keyword = keyword;
    }

    public ItemValidationResult(String keyword, String dataPath) {
        this.keyword = keyword;
        this.dataPath = dataPath;
    }

    public static ItemValidationResult build(String keyword, String dataPath) {
        return new ItemValidationResult(keyword, dataPath);
    }

    public static ItemValidationResult build(String keyword) {
        return new ItemValidationResult(keyword);
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDataPath() {
        if(this.getData() != null && this.getData().get("customDataPath") != null) {
            return this.getData().get("customDataPath");
        } else {
            return dataPath;
        }
    }

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }

    public String getSchemaPath() {
        return schemaPath;
    }

    public void setSchemaPath(String schemaPath) {
        this.schemaPath = schemaPath;
    }

    public Map<String, String> getParams() {
        return Optional.ofNullable(params).orElse(new HashMap<>());
    }

    /**
     * Get all params like a single formatted string
     *
     * @param format A {@link String.format()} valid format
     * @return A formatted {@link ItemValidationResult} string
     */
    public String getStringParams(String format) {

        StringBuilder strParams = new StringBuilder();
        Map<String, String> params = this.getParams();
        if (params.size() > 0) {
            strParams.append(",");
            params.forEach((key, value) -> strParams.append(String.format(format, key, value)));
        }

        return strParams.toString();
    }

    public String getStringParams() {
        return this.getStringParams(STRING_FORMAT_PARAMS);
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public String getCustomDataPath() {
        if (this.getData() != null) {
            return this.getData().get("customDataPath");
        }
        return null;
    }

    @Override
    public boolean equals(Object object) {
        boolean isSame = false;

        if (object != null && object instanceof ItemValidationResult) {
            ItemValidationResult objectToCompare = (ItemValidationResult) object;

            EqualsBuilder builder = new EqualsBuilder()
                .append(this.getKeyword(), objectToCompare.getKeyword());

            String dataPath = this.getDataPath();
            if(dataPath != null && dataPath.length() > 0) {

                builder.append(dataPath, objectToCompare.getDataPath());
            }

            isSame = builder.isEquals();
        }

        return isSame;
    }

    /**
     * A string representation of validation result
     *
     * @return A formatted string
     */
    @Override
    public String toString() {
        return this.toString(STRING_FORMAT, STRING_FORMAT_PARAMS);
    }

    /**
     * A string representation of validation result
     *
     * @param format A {@link String.format()} valid format
     * @return A formatted string
     */
    public String toString(String format, String formatParams) {
        String str = String.format(format, this.getSchemaPath(), this.getKeyword(), this.getMessage());
        return str + this.getStringParams(formatParams);
    }
}
