import { Component } from '@angular/core';

@Component({
  selector: 'npm-package2-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'double-agent-validator';
}
