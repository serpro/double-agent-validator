if (!Array.isArray) {
  (<any>Array)['isArray'] = function(arg: any) {
    return Object.prototype.toString.call(arg) === '[object Array]';
  };
}
