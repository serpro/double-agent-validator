import { Injectable } from '@angular/core';
import { RemoteLoader } from './models/remote-loader';

/**
 *
 * This class loads a script from an url, parses it and fill an ajv instance with theirs definitions
 */
@Injectable()
export class ValidatorDefinitionsLoader {
  _window: undefined | Window;

  get validatorExecutionContext(): Window {
    return this._window as Window;
  }

  /**
   * Creates an instance of ValidatorDefinitionsLoader.
   *
   */
  constructor(private remoteLoader: RemoteLoader) {}

  load(window: Window, url: string): Promise<void> {
    return this.remoteLoader.getScript(url).then(scriptContent => {
      return this.loadScript(window, scriptContent);
    });
  }

  private loadScript(window: Window, script: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      try {
        // handler to check any error on script evalution
        (window as any)['DoubleAgentValidatorErrorHandler'] = (e: any) => {
          reject(e);
        };

        window.document.write(`
          <script>
            try {
              ${script}
            } catch(e) {
              DoubleAgentValidatorErrorHandler(e);
            }
          </script>
        `);
        this._window = window;
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }
}
