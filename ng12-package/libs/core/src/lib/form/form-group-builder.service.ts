import { Injectable } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, AbstractControl, ValidatorFn, FormArray, AsyncValidatorFn } from '@angular/forms';
import { DoubleAgentValidator } from '../validator.service';
import { JsonSchema } from '../models/schema/json-schema';
import * as _ from 'lodash';
import { DoubleAgentFormControlValidatorBuilder } from './form-control-validator-builder.service';
import { DoubleAgentFormGroup } from './form-group';
import { DoubleAgentFormControl, DoubleAgentFormArray, DoubleAgentControl } from './form-control';

export interface FormGroupStates {
	[key: string]: FormControlState;
}

export type FormControlState = string | { value: string; disabled: boolean };

/**
 * This class allows creates a formGroup which contains all the fields represented in an given schema
 * each one containing it's own angular validators
 */
@Injectable()
export class DoubleAgentFormGroupBuilder {

	/**
	 * A json with values to "store" into FormGroup/FormArray's.
	 *
	 * These values are used to create initial FormGroups for
	 * FormArrays collections
	 */
	protected formValues: {[key: string]: any} = {};

	private config = {
		formGroupStates: {},
		createValidators: true,
		fillFormValues: false,
		initializeEmptyCollections: false
	};

	/**
   * Creates an instance of FormGroupBuilder.
   *
   */
	constructor(
		private doubleAgentValidator: DoubleAgentValidator,
		private formControlValidatorBuilder: DoubleAgentFormControlValidatorBuilder,
		private formBuilder: FormBuilder
	) { }

	setFormValues(data: { [key: string]: any }): DoubleAgentFormGroupBuilder {
		this.formValues = data;
		return this;
	}

	/**
   * Builds a FormGroup containing all the attributes defined in the jsonSchema
   *
   */
	build(
		schemaName: string,
		config?: {
			formGroupStates?: FormGroupStates;
			createValidators?: boolean,
			fillFormValues?: boolean,
			initializeEmptyCollections?: boolean
		}
	): DoubleAgentFormGroup | FormGroup {
		(this.config as any).formGroupStates = config ? config.formGroupStates : {};
		(this.config as any).createValidators = config ? config.createValidators : true;
		(this.config as any).fillFormValues = config ? config.fillFormValues : true;
		(this.config as any).initializeEmptyCollections = config ? config.initializeEmptyCollections : true;

		// TODO validar se o esquema existe e retornar erro apropriado
		const jsonSchema: JsonSchema = this.doubleAgentValidator.getSchema(schemaName);

		// if the schema does not have the id, set with the schemaName argument
		if (!jsonSchema.id) {
			jsonSchema.id = schemaName;
		}

		if (!jsonSchema) {
			throw Error(`Schema ${schemaName} not found!`);
		}

		// cria uma instância do FormGroup a partir da configuração construída
		const formGroupConfig = this.createControls(jsonSchema, this.formValues);
		const formGroup = <DoubleAgentFormGroup>this.formBuilder.group(formGroupConfig);

		// subscribe to valueChange of control if it has validateOnChange ui keyword
		_.each(formGroup.controls, (formControl: DoubleAgentFormControl) => {
			if ((formControl as any).jsonSchemaProperty['ui'] && (formControl as any).jsonSchemaProperty['ui']['validateOnChange']) {
				(formControl as any).valueChanges.subscribe(() => {
					_.each((formControl as any).jsonSchemaProperty['ui']['validateOnChange'], (propertyToValidate: string) => {
						if (formGroup.controls[propertyToValidate]) {
							formGroup.controls[propertyToValidate].updateValueAndValidity({ onlySelf: true });
						}
					});
				});
			}
		});

		if (this.config.createValidators) {
			// construir validador do FormGroup (keywords do objeto)
			this.addKeywordsValidator(jsonSchema, formGroup);
		}

		formGroup.jsonSchema = jsonSchema;

		// retorna o Form Group
		return formGroup;
	}

	/**
	 * Add FormGroups for FormArrays, if `values`
	 * param contains more data than 1
	 *
	 * @param formArray FormArray to add FormGroups
	 * @param values Json data verify the amount, and create FormGroups and/or fill their values
	 * @see DoubleAgentFormGroupBuilder.config.fillFormValues
	 */
	addFormArrayValues(formArray: DoubleAgentFormArray, values: {[key: string]: any}[]): DoubleAgentFormArray {
		if (values && values.length > 0) {
			values.forEach((groupValues, index) => {

				const group = formArray.at(index);

				if (!group) {
					formArray.addGroup();
				}

				if (this.config.fillFormValues) {
					formArray.at(index).setValue(groupValues);
				}
			});
		}

		return formArray;
	}

	private addKeywordsValidator(schema: JsonSchema, formGroup: FormGroup) {
		const keywords = this.doubleAgentValidator.getKeywords(schema);

		const validator = this.buildAngularFormGroupValidator(schema.id, keywords, formGroup);

		formGroup.setValidators([validator]);
	}

	/**
	 * Create controls and children FormGroup's from
	 * a jsonSchema
	 *
	 * @param jsonSchema
	 * @returns A object of controls/groups
	 * @see DoubleAgentValidator.getSchema
	 */
	createControls(jsonSchema: JsonSchema, values?: any): { [key: string]: DoubleAgentControl } {

		const formGroupConfig: { [key: string]: DoubleAgentControl } = {};

		_.each(jsonSchema.properties, (property: any, propertyName: string) => {

			if (property.skip) {
				return;
			}

			if (property.items || property.group) {

				const collectionControls = this.createControls(property.items || <any>property);
				const newGroup = (controls: { [key: string]: AbstractControl } = {}) => {
					controls = _.merge(_.cloneDeep(collectionControls), controls);
					return <DoubleAgentFormGroup>new FormGroup(controls);
				};

				const customFormArray = class extends FormArray implements DoubleAgentFormArray {
					constructor(
						controls: AbstractControl[],
						validator?: any,
						asyncValidator?: AsyncValidatorFn
					) {
						super(controls, validator, asyncValidator);
					}

					newGroup(controls: { [key: string]: AbstractControl } = {}) {
						return newGroup(controls);
					}

					addGroup(controls: { [key: string]: AbstractControl } = {}) {
						const group = this.newGroup(controls);
						this.push(group);
						return group;
					}
				}

				if (property.type === 'array' && property.groupArray) {
					const firsEmptyGroup = this.config.initializeEmptyCollections ? [newGroup()] : [];
					const formArray = <DoubleAgentFormArray>new customFormArray(firsEmptyGroup);
					if (values) {
						if (values instanceof Array) {
							values.forEach(innerValues => {
								if (innerValues[propertyName]) {
									this.addFormArrayValues(formArray, innerValues[propertyName]);
								}
							});
						}else if(values[propertyName] instanceof Array) {
							this.addFormArrayValues(formArray, values[propertyName]);
						}
					}

					formArray.jsonSchemaProperty = jsonSchema.properties[propertyName];
					formArray.propertyName = propertyName;
					formGroupConfig[propertyName] = formArray;
				} else {
					const formGroup = newGroup();
					formGroup.jsonSchemaProperty = jsonSchema.properties[propertyName];
					formGroup.propertyName = propertyName;
					formGroupConfig[propertyName] = formGroup;
				}

			} else {

				if (!_.has('$ref', propertyName) || !_.has('$ref', `${propertyName}.properties`) || property.type === 'object') {
					// Igore the property if refer another object or is an object
					let formState: string | FormControlState = '';
					if (this.config.formGroupStates && (this.config as any).formGroupStates[propertyName]) {
						formState = (this.config as any).formGroupStates[propertyName];
					}
					const formControl: DoubleAgentFormControl =
						(this.config as any) && (this.config as any).createValidators
							? <DoubleAgentFormControl>new FormControl(
								formState,
								this.formControlValidatorBuilder.build(jsonSchema, propertyName)
							)
							: <DoubleAgentFormControl>new FormControl(formState);
					formControl.jsonSchemaProperty = jsonSchema.properties[propertyName];
					formControl.propertyName = propertyName;
					formGroupConfig[propertyName] = formControl;
				}
			}
		});

		return formGroupConfig;
	}

	buildAngularFormGroupValidator(schemaName: string, keywords: string[], formGroup: FormGroup): ValidatorFn {
		return (control: AbstractControl) => {
			const validationResult = {
				jsonSchema: null,
			};

			const data = formGroup.value;

			// runs the validation
			const result = this.doubleAgentValidator.validate(schemaName, data);

			if (result.hasErrors) {
				const errorsOfKeyword = _.filter(result.errors, (error: any) => {
					return _.includes(keywords, error.keyword);
				});
				if (errorsOfKeyword.length > 0) {
					(validationResult as any).jsonSchema = {
						errors: errorsOfKeyword,
					};
					return validationResult;
				}
			}
			// retorna null no caso de não ter erros de validacao
			return null;
		};
	}
}
