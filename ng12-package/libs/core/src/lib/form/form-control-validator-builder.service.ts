import { Injectable } from '@angular/core';
import { AbstractControl, Validators, ValidatorFn } from '@angular/forms';
import { JsonSchema } from '../models/schema/json-schema';
import { DoubleAgentValidator } from '../validator.service';
import * as _ from 'lodash';
import { DoubleAgentFormControl } from './form-control';

@Injectable()
export class DoubleAgentFormControlValidatorBuilder {

  constructor(private doubleAgentValidator: DoubleAgentValidator) {
  }

  /**
   * Returns the validator representing some attribute in the schema
   * which will be used to Angular 2 validates the user input
   */
  build(schema: JsonSchema, property: string): ValidatorFn {
    const validators: ValidatorFn[] = [];

    if (schema.required && schema.required.indexOf(property) >= 0) {
      validators.push(Validators.required);
    }
    validators.push(this.buildAngularValidator(schema.id, property));
    return Validators.compose(validators) as ValidatorFn;
  }

  /**
   * This creates a Angular validator corresponding a json schema and optionally
   * only for a given property
   *
   */
  buildAngularValidator(schemaName: string, propertyOrFormData?: string | Record<string, unknown>): any {
    return (control: AbstractControl) => {
      const validationResult = {
        jsonSchema: null
      };


      // builds the data which will be validated
      let data: Record<string, unknown> = {};
      if (_.isString(propertyOrFormData)) {
        data[propertyOrFormData as string] = control.value;
        const jsonSchemaFormControl = <DoubleAgentFormControl>control;
        if (jsonSchemaFormControl.jsonSchemaProperty &&
          jsonSchemaFormControl.jsonSchemaProperty.ui &&
          jsonSchemaFormControl.jsonSchemaProperty.ui.dependents && control.root && (control.root as any)['controls']) {
          _.each(jsonSchemaFormControl.jsonSchemaProperty.ui.dependents, function (propertyName: string) {
            data[propertyName] = (control.root as any)['controls'][propertyName].value;
          });
        }

      } else {
        data = propertyOrFormData as Record<string, unknown>;
      }

      // runs the validation
      const result = this.doubleAgentValidator.validate(schemaName, data);
      if (result.hasErrors) {


        // if a specific property was provided, then only returns error refering that property
        if (_.isString(propertyOrFormData)) {
          const errorsOfProperty = _.filter(result.errors, function (error: any) {
            return error.dataPath.match("." + propertyOrFormData);
          });
          if (errorsOfProperty.length > 0) {
            (validationResult as any).jsonSchema = {
              errors: errorsOfProperty
            };
            return validationResult;
          } else {
            return null;
          }
        } else {
          // if no specific property was passed, so return all errors found
          (validationResult as any).jsonSchema = {
            errors: result.errors
          };
          return validationResult;
        }
      } else {
        // retorna null no caso de não ter erros de validacao
        return null;

      }
    };
  }
}
