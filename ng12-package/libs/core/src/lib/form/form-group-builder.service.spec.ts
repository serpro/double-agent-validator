import { DoubleAgentFormGroupBuilder } from './form-group-builder.service';
import { DoubleAgentFormArray } from './form-control';
import { DoubleAgentValidator } from '../validator.service';
import * as jsdomNS from 'jsdom';
import { getDoubleAgentMockedScriptContent } from '../directives/mock';
import { DoubleAgentFormControlValidatorBuilder } from './form-control-validator-builder.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

describe(DoubleAgentFormGroupBuilder.name, () => {
  let subject: DoubleAgentValidator;
  let scriptContent = getDoubleAgentMockedScriptContent();
  let window: Window;
  let formGroupBuilder: DoubleAgentFormGroupBuilder;

  beforeAll((done) => {
    if (typeof window === 'undefined') {
      const objDOM = new jsdomNS.JSDOM(
        '<html><body>Página de Teste<script>' +
          scriptContent +
          '</script></body></html>',
        { runScripts: 'dangerously', url: 'http://localhost' }
      );
      window = objDOM.window;
    }

    window.onload = ev => {
      subject = new DoubleAgentValidator();
      subject['scriptContext'] = window;
      done();
    };
  });


  beforeEach(() => {
    formGroupBuilder = new DoubleAgentFormGroupBuilder(subject, new DoubleAgentFormControlValidatorBuilder(subject), new FormBuilder());
  });

  it('builds a formGroup adding validators', () => {
    let formGroup = formGroupBuilder.build('contribuinte-v1');
    expect(formGroup.get('ni').validator).not.toBeNull();
  });

  it('builds a formGroup with nested jsonSchema that contains extra properties or collection', () => {
    let formGroup = formGroupBuilder.build('contribuinte-v1');
    const collection = <FormArray>formGroup.get('collection');
    const collectionTwo = <FormGroup>formGroup.get('collectionTwo');

    expect(collection).toBeInstanceOf(FormArray);
    expect(collection['newGroup']).toBeInstanceOf(Function);

    collection.push(collection['newGroup']());
    collection.setValue([
      {
        id: 1,
        value: '100'
      },
      {
        id: 2,
        value: '200'
      }
    ]);

    expect(collection.value).toContainEqual({ id: 1, value: '100' });

    expect(collectionTwo).toBeInstanceOf(FormGroup);
    expect(collectionTwo.contains('otherValue')).toBeTruthy();

  });

  it('verify if formArray values not contains the same reference', () => {
    let formGroup = formGroupBuilder.build('contribuinte-v1');
    const collection = <DoubleAgentFormArray>formGroup.get('collection');

    collection.setValue([
      {
        id: 1,
        value: '100'
      }
    ]);

    expect(collection.value).toContainEqual({ id: 1, value: '100' });

    collection.addGroup();
    expect(collection.length).toEqual(2);
    expect(collection.value[1]).toEqual({ id: '', value: '' });

  });

  it('verify if formArray values match with parent formGroup values', () => {
    const initialFormValues = {
      'collection': [
        { id: 1, value: '300'},
        { id: 2, value: '400'}
      ]
    };

    let formGroup = formGroupBuilder.setFormValues(initialFormValues).build('contribuinte-v1');
    const collection = <DoubleAgentFormArray>formGroup.get('collection');

    expect(collection.controls.length).toEqual(initialFormValues.collection.length);
    expect(collection.value[0]).toEqual({ id: 1, value: '300' });
    expect(collection.value).toContainEqual({ id: 2, value: '400' });

  });

  it('not builds a formGroup if the property "skipForm" is present', () => {
    let formGroup = formGroupBuilder.build('contribuinte-v1');
    const skiped = <FormGroup>formGroup.get('skipedControl');
    expect(skiped).toBeNull();
  });

  it('builds a formGroup without validators', () => {
    let formGroup = formGroupBuilder.build('contribuinte-v1', { createValidators: false });
    expect(formGroup.get('ni').validator).toBeNull();
  });

});
