import { DoubleAgentAbstractControl } from './abstract-control';
import { FormGroup } from '@angular/forms';
import { JsonSchema } from '../models/schema/json-schema';

export interface DoubleAgentFormGroup extends FormGroup, DoubleAgentAbstractControl {
  jsonSchema: JsonSchema;
}
