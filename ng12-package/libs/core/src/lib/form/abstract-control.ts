import { JsonSchemaProperty } from './../models/schema/json-schema-property';

export interface DoubleAgentAbstractControl {
  propertyName?: string;
  jsonSchemaProperty?: JsonSchemaProperty;
}
