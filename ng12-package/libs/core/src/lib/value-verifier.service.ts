import { Injectable } from '@angular/core';
import { DoubleAgentValidator } from './validator.service';
import { DoubleAgentFormGroup } from './form/form-group';
import { FormControlName } from '@angular/forms';

@Injectable()
export class DoubleAgentValueVerifier {
  constructor(private doubleAgentValidator: DoubleAgentValidator) {}

  filter(formControlName: FormControlName, value: any): any {
    const schemaName = (<any>formControlName.control.parent)
      .parent['schemaName'];
    const propertyName = formControlName.name as string;

    const schema = this.doubleAgentValidator.getSchema(schemaName);
    const property = schema.properties[propertyName];

    if (property) {
      if (property['pattern'] || property['format']) {
        const formatName = property['format'];
        let pattern: RegExp | null | undefined = null;
        if (formatName) {
          pattern = this.doubleAgentValidator.getFormats()[formatName];
        } else {
          pattern = property['pattern'];
        }

        if (pattern != null && pattern != undefined && pattern.test(value)) {
          return value;
        } else {
          return null;
        }
      }
      return value;
    }
  }
}
