import { ValidationResult } from './models/validation-result';
import Ajv from 'ajv';
import { JsonSchema } from './models/schema/json-schema';
import { Injectable } from '@angular/core';

import { ReplaySubject } from 'rxjs';
import { Validator } from './models/validator';

@Injectable()
export class DoubleAgentValidator implements Validator {
  private scriptContext: unknown;

  isReady: ReplaySubject<void> = new ReplaySubject<void>(1);


  get ajv(): Ajv {
    return (this.scriptContext as any)['ajv'];
  }

  validate(
    schemaName: string,
    data: any,
    options: { removeEmptyStrings: boolean; parseResultFn?: (obj: any) => any } = {
      removeEmptyStrings: true
    }
  ): ValidationResult {
    const result: ValidationResult = (this.scriptContext as any) ['DoubleAgent'][
      'JsonSchemaValidator'
    ].validate(schemaName, data, options);
    return result;
  }

  getSchema(schemaName: string): JsonSchema {
    return <JsonSchema>(this.scriptContext as any)['DoubleAgent'][
      'JsonSchemaValidator'
    ].getSchemaObject(schemaName);
  }

  getKeywords(schema: JsonSchema): string[] {
    return (this.scriptContext as any)['DoubleAgent']['JsonSchemaValidator'].getKeywords(
      schema
    );
  }

  getFormats(): any {
    return (this.scriptContext as any)['DoubleAgent'][
      'JsonSchemaValidator'
    ].getFormats();
  }

  getFormat(format: string): any {
    return (this.scriptContext as any)['DoubleAgent']['JsonSchemaValidator'].getFormat(
      format
    );
  }

  get schemasNames(): string[] {
    return (this.scriptContext as any)['DoubleAgent'][
      'JsonSchemaValidator'
    ].getSchemas();
  }
}
