import { Directive, OnInit, Renderer2, ElementRef } from '@angular/core';
import { FormControlName } from '@angular/forms';
import { JsonSchemaProperty } from '../models/schema/json-schema-property';
import { DoubleAgentFormControl } from '../form/form-control';

/**
 * The FormControls created with the DoubleAgentValidatorFormBuilder
 * will have properties defined in the keyworkd maxLength or ui.maxLength
 * or minLength or ui.minLength reflected to the input field associated to the formControlName
 */
@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[formControlName]'
})
export class InputHtml5AttributesDirective implements OnInit {
  private controlJsonProperty: JsonSchemaProperty | undefined | null = null;

  constructor(
    private renderer: Renderer2,
    private formControlName: FormControlName,
    private elementRef: ElementRef
  ) {
  }

  findInputElement(): HTMLInputElement  | null {
    if ((<Element> this.elementRef.nativeElement).nodeName === 'INPUT') {
      return this.elementRef.nativeElement;
    } else {
      const rootElement = <Element> this.elementRef.nativeElement;
      const elementChildren = Array.from(rootElement.children);
      for (const el of elementChildren) {
        if (el.nodeName  === 'INPUT') {
          return <HTMLInputElement> el;
        }
      }
      return null;
    }
  }

  ngOnInit() {
    this.controlJsonProperty = (<DoubleAgentFormControl>this.formControlName.control).jsonSchemaProperty;
    if (!this.controlJsonProperty) { return; } // return if does not have jsonSchemaProperty defined

    const element = this.findInputElement();
    if (!element) { return; } // return if the element is not a input

    if (this.controlJsonProperty.maxLength) {
      this.renderer.setAttribute(
        element,
        'maxlength',
        this.controlJsonProperty.maxLength.toString()
      );
    }

    if (this.controlJsonProperty['ui'] && this.controlJsonProperty['ui']['maxLength']) {
      this.renderer.setAttribute(
        element,
        'maxlength',
        this.controlJsonProperty['ui']['maxLength']
      );
    }

    if (this.controlJsonProperty.minLength) {
      this.renderer.setAttribute(
        element,
        'minlength',
        this.controlJsonProperty.minLength.toString()
      );
    }

    if (this.controlJsonProperty['ui'] && this.controlJsonProperty['ui']['minLength']) {
      this.renderer.setAttribute(
        element,
        'minlength',
        this.controlJsonProperty['ui']['minLength']
      );
    }

  }
}
