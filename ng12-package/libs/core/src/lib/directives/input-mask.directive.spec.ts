import { Component, Renderer, DebugElement } from '@angular/core';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';
import { getTestBed, TestBed, ComponentFixture } from '@angular/core/testing';
import { InputMaskDirective } from './input-mask.directive';
import { DoubleAgentFormGroupBuilder } from '../form/form-group-builder.service';
import { DoubleAgentValidator } from '../validator.service';
import { DoubleAgentFormControlValidatorBuilder } from '../form/form-control-validator-builder.service';
import * as jsdomNS from 'jsdom';
import { triggerInput, getFormElement } from './test-helpers';
import { getDoubleAgentMockedScriptContent } from './mock';

let scriptContent = getDoubleAgentMockedScriptContent();

let window: Window;

let doubleAgentValidator: DoubleAgentValidator;

describe(`InputMaskDirective`, () => {


  @Component({
    selector: 'not-used',
    template: `
    <form [formGroup]=form>
      <input type="text" formControlName="area" >
      <input type="text" formControlName="nome" >
      <input type="text" formControlName="ni" >
      <input type="text" formControlName="cnpj"/>
      <input type="text" formControlName="cnpjAndCpf"/>
      <input type="text" formControlName="cep" >
      <input type="text" formControlName="valor" >
      <input type="text" formControlName="telefone" >
      <input type="text" formControlName="nitPisPasep" >
      <input type="text" formControlName="tituloEleitoral" >
      <input type="text" formControlName="ddi" >
      <input type="text" formControlName="tel" >
      <input type="text" formControlName="negativo" >
      <input type="text" formControlName="cepDoNotMask" doNotMask>
      <input type="text" formControlName="cepbsDatepicker" bsDatepicker>
    </form>
    `
  })
  class MyComponent {
    form: FormGroup;
    constructor(doubleAgentFormGroupBuilder: DoubleAgentFormGroupBuilder) {
      this.form = doubleAgentFormGroupBuilder.build('contribuinte-v1');
    }
  }


  let fixture: ComponentFixture<MyComponent>;

  beforeAll((done) => {
    const objDOM = new jsdomNS.JSDOM(
      '<html><body>Página de Teste<script>' +
        scriptContent +
        '</script></body></html>',
      { runScripts: 'dangerously', url: 'http://localhost' }
    );

    window = objDOM.window;
    window.onload = ev => {
      doubleAgentValidator = new DoubleAgentValidator();
      doubleAgentValidator['scriptContext'] = window;
      done();
    };
  });
  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        MyComponent,
        InputMaskDirective
      ],
      imports: [
        ReactiveFormsModule
      ],
      providers: [
        { provide: DoubleAgentValidator, useValue: doubleAgentValidator },
        DoubleAgentFormGroupBuilder,
        DoubleAgentFormControlValidatorBuilder,
        Renderer
      ]
    });
    fixture = TestBed.createComponent(MyComponent);
  });

  afterEach(() => {
    getTestBed().resetTestingModule();
  });

  // should do nothing as field "nome" doesn't have a mask
  it('mask "nome" value', () => {
    let field = triggerInput(fixture, {
                  fieldName: 'nome',
                  value: 'Fulano de Tal'
                });
    expect(field.input.value).toEqual('Fulano de Tal');
  });

  // TODO: Dynamic masks not working on Angular 6 version
  xit('mask "cnpj" value', () => {
    const field = triggerInput(fixture, {
      fieldName: 'ni',
      value: '00000000000191'
    });
    expect(field.input.value).toEqual('00.000.000/0001-91');
  });

  // TODO: Dynamic masks not working on Angular 6 version
  xit('mask "cpf" value', () => {
    const field = triggerInput(fixture, {
      fieldName: 'ni',
      value: '00000000191'
    });
    expect(field.input.value).toEqual('000.000.001-91');
  });

  // TODO: Dynamic masks not working on Angular 6 version
  xit('mask from "cpf" to "cnpj" value', () => {
    const field = triggerInput(fixture, {
      fieldName: 'ni',
      value: '99999999999'
    });

    expect(field.input.value).toEqual('999.999.999-99');

    field.triggerInput('99999999999999');
    fixture.detectChanges();

    expect(field.input.value).toEqual('99.999.999/9999-99');
  });


  it('mask "valor" value', () => {
    let field =  triggerInput(fixture, {
                  fieldName: 'valor',
                  value: '12345678'
                });
    expect(field.input.value).toEqual('123.456,78');
  });

  it('mask very large "valor" value', () => {
    let field =  triggerInput(fixture, {
                  fieldName: 'valor',
                  value: '9999999999999'
                });
    expect(field.input.value).toEqual('99.999.999.999,99');
  });

  it('mask "telefone" 8 digits', () => {
    let field =  triggerInput(fixture, {
                  fieldName: 'telefone',
                  value: '9999999999'
                });
    expect(field.input.value).toEqual('(99) 9999-9999');
  });

  it('mask "telefone" 9 digits', () => {
    let field =  triggerInput(fixture, {
                  fieldName: 'telefone',
                  value: '99999999999'
                });
    expect(field.input.value).toEqual('(99) 99999-9999');
  });

  it('mask "nitPisPasep" value', () => {
    let field =  triggerInput(fixture, {
                  fieldName: 'nitPisPasep',
                  value: '99999999999'
                });
    expect(field.input.value).toEqual('999.99999.99-9');
  });

  it('mask "tituloEleitoral" value', () => {
    let field =  triggerInput(fixture, {
                  fieldName: 'tituloEleitoral',
                  value: '9999999999999'
                });
    expect(field.input.value).toEqual('9999 9999 99999');
  });

  it('mask "CEP" value', () => {
    let field =  triggerInput(fixture, {
                  fieldName: 'cep',
                  value: '99999999'
                });
    expect(field.input.value).toEqual('99999-999');
  });

  fit('do Not mask "CEP" value if selector doNotMask is present', () => {
    const element: DebugElement = getFormElement(fixture, "cepDoNotMask");
    expect(() => element.injector.get<InputMaskDirective>(InputMaskDirective)).toThrowError();
  });

  fit('do Not mask "CEP" value if selector bsDatepicker is present', () => {
    const element: DebugElement = getFormElement(fixture, "cepbsDatepicker");
    expect(() => element.injector.get<InputMaskDirective>(InputMaskDirective)).toThrowError();
  });

  // TODO: Dynamic masks not working on Angular 6 version
  xit('mask "ddi" value', () => {
    let field =  triggerInput(fixture, {
                  fieldName: 'ddi',
                  value: '9999'
                });

    expect(field.input.value).toEqual('9999');
  });

  it('mask fixed "cnpj" value', () => {
    let field = triggerInput(fixture, {
                  fieldName: 'cnpj',
                  value: '00000000000191'
                });
    expect(field.input.value).toEqual('00.000.000/0001-91');
    expect(field.maskDirective.formControl.jsonSchemaProperty['maxLength']).toEqual(18);
  });

  it('mask dinamically change from "cnpj" to "cpf" format value', () => {
    let field = triggerInput(fixture, {
                  fieldName: 'cnpjAndCpf',
                  value: '00000000000191',
                  maskFormat: 'cnpj'
                });
    expect(field.input.value).toEqual('00.000.000/0001-91');
    expect(field.maskDirective.formControl.jsonSchemaProperty['maxLength']).toEqual(18);

    field.triggerInput('99999999999', 'cpf');
    fixture.detectChanges();

    expect(field.input.value).toEqual('999.999.999-99');
    expect(field.maskDirective.formControl.jsonSchemaProperty['maxLength']).toEqual(14);

    /*
    * Don't specify a format. Will be defined by `ui.mask[].matcher`
    * or `ui.mask[].format` regex that match with field value
    */
    field.triggerInput('00000000000191');
    fixture.detectChanges();

    expect(field.input.value).toEqual('00.000.000/0001-91');
    expect(field.maskDirective.formControl.jsonSchemaProperty['maxLength']).toEqual(18);
  });

  it('masks currency with precision 1', () => {
    let field = triggerInput(fixture, {
      fieldName: 'area',
      value: '1209'
    });
    expect(field.input.value).toEqual('120,9');
  });

  it('masks currency with negative number', () => {
    let field = triggerInput(fixture, {
      fieldName: 'negativo',
      value: '-1234'
    });

    expect(field.input.value).toEqual('-12,34');
  });

});
