import { DoubleAgentValidator } from './validator.service';
import { ValidatorDefinitionsLoader } from './definitions-loader.service';
import { Injectable, Injector } from '@angular/core';
import { RemoteLoader } from './models/remote-loader';

/**
 *
 * This classs provide a facility to load json schema definitions into a DoubleAgentValidator instance and provide it
 * to Angular Dependency Injection
 */
// @dynamic
@Injectable()
export class DoubleAgentValidatorNg2Factory {
  /**
   * This static function is utilized as a provider Factory to builds the DoubleAgentValidator
   * instance filled with json schemas from an given url into the Angular2  dependency injection
   */
  static factoryFn(
    injector: Injector,
    factory: DoubleAgentValidatorNg2Factory,
    urlSchema: string
  ) {
    return new Promise<void>(function(resolve, reject) {
      let errors = null;
      if (urlSchema == null) {
        errors =
          'DoubleAgentValidator Module needs an url provided through the DOUBLE_AGENT_VALIDATOR_SCHEMA_URL token';
      }
      if (errors) {
        reject(errors);
      }
      factory
        .load(urlSchema)
        .then(function() {
          resolve();
        })
        .catch(function() {
          reject();
        });
    });
    // }
  }

  /**
   * Creates an instance of DoubleAgentValidatorNg2Factory.
   */
  constructor(
    private doubleAgentValidator: DoubleAgentValidator,
    private remoteLoader: RemoteLoader
  ) {}

  /**
   * Loads a script from a url, parses it and load into the ajv object.
   * At this moment is using a iframe to isolate the parse/evaluate of the code.
   * Maybe it would useful have a strategy loading using web worker
   */
  load(url: string): Promise<void> {
    const validationsLoader = new ValidatorDefinitionsLoader(this.remoteLoader);
    const iframe = document.createElement('iframe');
    iframe.id = 'DoubleAgentValidator';
    iframe.style.border = '0';
    iframe.src = 'about:blank';
    iframe.style.background = 'transparent';
    iframe.style.display = 'none';
    iframe.style.width = '1px';
    iframe.style.height = '1px';
    (<any>iframe).sandbox = 'allow-scripts allow-same-origin allow-modals';
    document.body.appendChild(iframe);
    const window = iframe.contentWindow;
    const doubleAgentValidator = this.doubleAgentValidator;
    return new Promise<void>(function(resolve, reject) {
      validationsLoader.load((window as any), url).then(
        () => {
          doubleAgentValidator['scriptContext'] = window;
          (window as any)['DoubleAgentValidator'] = doubleAgentValidator;
          // this.doubleAgentValidator['_notifyReady']();
          resolve();
        },
        e => {
          reject('Could not create the DoubleAgentValidator instance: ' + e);
        }
      );
    });
  }
}
