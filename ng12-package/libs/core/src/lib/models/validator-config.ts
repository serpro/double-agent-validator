import { ValidationFn } from './validation-function';
import { Injectable } from '@angular/core';

Injectable()
export class DoubleAgentValidatorConfig {
  constructor(public ajv: any,  public validationFn: ValidationFn) {}
}
