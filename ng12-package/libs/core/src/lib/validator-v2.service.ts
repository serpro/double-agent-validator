import Ajv from 'ajv';
import { Validator } from './models/validator';
import { ValidationResult } from './models/validation-result';
import { JsonSchema } from './models/schema/json-schema';
import { Injectable } from '@angular/core';
import { DoubleAgentValidatorConfig } from './models/validator-config';

@Injectable()
export class DoubleAgentValidatorV2 implements Validator {
  constructor(private config: DoubleAgentValidatorConfig) {}

  get ajv(): Ajv {
    if (!this.config || (this.config && !this.config.ajv)) {
      throw new Error('Ajv instance is missing in DoubleAgentValidatorV2');
    }
    return (this.config as any).ajv;
  }

  get schemasNames(): string[] {
    return [];
  }

  validate(schemaName: string, data: any): ValidationResult | Promise<any> {
    let errors: any;

    if (this.config && this.config.validationFn) {
      errors = this.config.validationFn(schemaName, data);
      if (errors && typeof errors.hasErrors !== 'undefined') {
        return errors;
      }
    } else {
      this.ajv.validate(schemaName, data);
      errors = this.ajv.errors;
    }

    if (errors && errors.then) {
      return <Promise<any>>errors;
    } else {
      return {
        hasErrors: !!errors,
        errors: errors
      };
    }
  }

  getSchema(schemaName: string): JsonSchema {
    return (this.ajv as any)['_schemas'][schemaName].schema;
  }

  getKeywords(schema: JsonSchema): string[] {
    return (this.ajv as any)['RULES'].all;
  }
  getFormats(): any {
    return (this.ajv as any)['_formats'];
  }

  getFormat(format: string): any {
    return (this.ajv as any)['_formats'][format];
  }
}
