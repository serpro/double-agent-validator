import { DoubleAgentValidator } from './validator.service';
import * as jsdomNS from 'jsdom';

import { getDoubleAgentMockedScriptContent } from './directives/mock';

describe('DoubleAgentValidator', () => {
  let subject: DoubleAgentValidator;

  const scriptContent = getDoubleAgentMockedScriptContent();
  let window: Window;

  beforeEach(done => {
    const objDOM = new jsdomNS.JSDOM(
      '<html><body>Página de Teste<script>' +
        scriptContent +
        '</script></body></html>',
      { runScripts: 'dangerously', url: 'http://localhost' }
    );

    window = objDOM.window;

    window.onload = ev => {
      subject = new DoubleAgentValidator();
      subject['scriptContext'] = window;
      done();
    };
  });

  it('hasErrors return true when validation fails', () => {
    expect(subject.validate('contribuinte-v1', {}).hasErrors).toEqual(true);
  });
});
