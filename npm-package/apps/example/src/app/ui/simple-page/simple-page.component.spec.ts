import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimplePageComponent } from './simple-page.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { ComponentsComponent } from '../../components/components.component';

describe('SimplePageComponent', () => {
  let component: SimplePageComponent;
  let fixture: ComponentFixture<SimplePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimplePageComponent ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({

            }),
            params: of({
              contentComponent: ComponentsComponent
            }),
            snapshot: {
              params: of({
                contentComponent: ComponentsComponent
              })
            }
          }
        }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimplePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
