import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver, Inject } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { AbstractContentComponent } from '../base/abstract-content.component';
import { Observable, pipe, forkJoin } from 'rxjs';

@Component({
  selector: 'doubleagent-simple-page',
  templateUrl: './simple-page.component.html',
  styleUrls: ['./simple-page.component.css']
})
export class SimplePageComponent implements OnInit {

  title = 'Not defined';

  @ViewChild('content', { read: ViewContainerRef }) viewContainerRef: ViewContainerRef;
  componentInstance: AbstractContentComponent;
  private data: any;

  private constructedForParams: string;

  constructor(
    private route: ActivatedRoute,
    private factoryResolver: ComponentFactoryResolver
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.data = data;
      this.constructComponent(this.route.snapshot.params);
    })
    this.route.params.subscribe(params => this.constructComponent(params));
  }

  private constructComponent(params?: any ) {
    if (JSON.stringify(params) !== this.constructedForParams) {
      this.title = this.data.title;
      const factory = this.factoryResolver.resolveComponentFactory<AbstractContentComponent>(this.data.contentComponent);
      const component  = factory.create(this.viewContainerRef.parentInjector);
      this.componentInstance = component.instance;
      this.viewContainerRef.clear();
      this.viewContainerRef.insert(component.hostView);
      this.constructedForParams = JSON.stringify(params);
    } else {
      console.log('Already constructed!');
    }
  }

}
