

export abstract class AbstractContentComponent {
    title: string;
    constructionDateTime: string;

    constructor() {
        this.constructionDateTime = new Date().getTime().toString();
    }
}