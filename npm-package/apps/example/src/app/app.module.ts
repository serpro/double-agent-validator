import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NxModule } from '@nrwl/nx';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { appRoutes } from './app.routes';
import { UIModule } from './ui/ui.module';
import { ComponentsComponent } from './components/components.component';


@NgModule({
  declarations: [AppComponent, HomeComponent, ComponentsComponent],
  imports: [
    BrowserModule,
    NxModule.forRoot(),
    RouterModule.forRoot(appRoutes, { initialNavigation: 'enabled' }),
    UIModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
