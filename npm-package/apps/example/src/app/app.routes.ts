import { HomeComponent } from './home/home.component';
import { Routes } from '@angular/router';
import { ComponentsComponent } from './components/components.component';
import { SimplePageComponent } from './ui/simple-page/simple-page.component';

export const appRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        data: {
            title: 'Início'
        }
    },
    {
        path: 'forms',
        component: HomeComponent,
        data: {
            title: 'Formulários'
        }
    },
    {
        path: 'services',
        component: HomeComponent,
        data: {
            title: 'Serviços'
        }
    },
    {
        path: 'components',
        component: SimplePageComponent,
        data: {
            title: 'Componentes',
            contentComponent: ComponentsComponent
        }
    }
];