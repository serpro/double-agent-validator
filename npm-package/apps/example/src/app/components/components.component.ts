import { Component, OnInit } from '@angular/core';
import { AbstractContentComponent } from '../ui/base/abstract-content.component';

@Component({
  selector: 'doubleagent-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.css']
})
export class ComponentsComponent extends AbstractContentComponent implements OnInit {
  field1 = 'Field content';
  constructor() {
    super();
  }

  ngOnInit() {
  }

}
