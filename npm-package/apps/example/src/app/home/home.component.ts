import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'doubleagent-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title =  'Home Component';
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.subscribe(r => {
      this.title = r.title;
    });
  }
}
