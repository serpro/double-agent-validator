import { Component } from '@angular/core';
import { appRoutes } from './app.routes';

@Component({
  selector: 'double-agent-validator-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'double-agent-validator';

  menuFromRoutes = appRoutes.map(route => ({
    title: route.data.title,
    path: [route.path]
  }));

}
