module.exports = {
  automock: false,
  "globals": {
    "__TS_CONFIG__": "<rootDir>libs/double-agent-validator/src/tsconfig.spec.json",
    "__TRANSFORM_HTML__": true
  },
  "preset": "jest-preset-angular",
  "setupTestFrameworkScriptFile": "<rootDir>/tools/jest/setupJest.ts",
  // snapshotSerializers: [
  //   '<rootDir>/node_modules/enzyme-to-json/serializer'
  // ],
  moduleNameMapper: {
    '^.+\\.(css|scss)$': 'identity-obj-proxy'
  }
}
