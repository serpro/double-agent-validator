
import * as Ajv from 'ajv';

export const ajv: Ajv.Ajv = new Ajv({
  allErrors: true,
  coerceTypes: true,
  $data: true
});

ajv.addSchema({
  type: 'object',
  required: ['name'],
  properties: {
    'name': {
      type: 'string'
    },
    'investments': {
      type: 'number',
      ui: {
        mask: '99,99'
      }
    }

  }
}, 'person');

export function validationFn(schemaName: string, data: any): any {
  let result = ajv.validate(schemaName, data);
  return {
    hasErrors: !result,
    errors: ajv.errors
  };
}
