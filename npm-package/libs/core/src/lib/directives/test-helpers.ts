import { DebugElement } from '@angular/core';
import { ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { InputMaskDirective } from './input-mask.directive';

export interface InputOptions {
  fieldName: string;
  maskFormat?: string;
  element?: DebugElement;
}

export interface InputTriggerOptions extends InputOptions {
  value: string;
}

export function getFormElement(fixture: ComponentFixture<any>, controlName: string) {
  return fixture.debugElement.query(By.css(`input[formcontrolname=${controlName}]`));
}

export function getMockForMask(fixture: ComponentFixture<any>, options: InputOptions) {
  const element: DebugElement = options.element || getFormElement(fixture, options.fieldName);
  const maskDirective: InputMaskDirective = element.injector.get(InputMaskDirective);

  if (maskDirective && options.maskFormat) {
    maskDirective.formControl.jsonSchemaProperty.format = options.maskFormat;
  }

  let mock = {
    maskDirective: maskDirective,
    element: element,
    input: <HTMLInputElement>element.nativeElement,
    triggerInput: (inputValue: string, maskFormat: string = null) => {

      if (maskFormat) {
        mock.maskDirective.formControl.jsonSchemaProperty.format = maskFormat;
      } else if (mock.maskDirective.formControl.jsonSchemaProperty.format) {
        delete mock.maskDirective.formControl.jsonSchemaProperty.format;
      }

      mock.input.value = inputValue;
      element.triggerEventHandler('input', { target: mock.input });
    }
  };

  return mock;
}

export function triggerInput(fixture: ComponentFixture<any>, options: InputTriggerOptions) {
  fixture.detectChanges();
  let fieldWithMask = getMockForMask(fixture, options);
  fieldWithMask.triggerInput(options.value);
  fixture.detectChanges();
  return fieldWithMask;
}
