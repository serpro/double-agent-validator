import { Directive, HostListener, Injector, Renderer, AfterViewInit } from '@angular/core';
import { FormControlName } from '@angular/forms';

import * as VMaskerImported from 'vanilla-masker';

import { DoubleAgentFormControl } from '../form/form-control';
import { findInArray } from '../helpers';
import { DoubleAgentValidator } from '../validator.service';

const VMasker = VMaskerImported;

/**
 * Applies a mask to user input following the configuration
 * passed-in the json-schema which originated the formControlName (if it is the case)
 *
 */
@Directive({
  selector: '[formControlName]:not([bsDatepicker]) [formControlName]:not([doNotMask])'
})
export class InputMaskDirective implements AfterViewInit {

  formControl: DoubleAgentFormControl;

  origiMaxLength: number = 0;

  constructor(
    private injector: Injector,
    private renderer: Renderer
  ) { }

  ngAfterViewInit() {
    this.formControl = <DoubleAgentFormControl>this.formControlName().control;
  }

  formControlName(): FormControlName {
    return <FormControlName>this.injector.get(FormControlName);
  }

  doubleAgent(): DoubleAgentValidator {
    return <DoubleAgentValidator>this.injector.get(DoubleAgentValidator);
  }

  getUiMask(input?: HTMLInputElement) {

    let jsonProperty = this.formControl.jsonSchemaProperty;
    if (jsonProperty && jsonProperty['ui']) {
      if (Array.isArray(jsonProperty['ui']['mask'])) {
        let masksArray: any[] = <any[]>jsonProperty['ui']['mask'];
        VMasker(input).unMask();

        if (jsonProperty.maxLength > 0 && this.origiMaxLength === 0) {
          this.origiMaxLength = jsonProperty.maxLength;
        }
        if (jsonProperty.format) {
          const formatRegex = this.doubleAgent().getFormat(jsonProperty.format);

          if (formatRegex) {
            masksArray = masksArray.filter((mask) => {
              const hasFormat: boolean = mask.format && mask.format == jsonProperty.format;
              if (hasFormat) {
                mask.matcher = formatRegex;
                this.formControl.jsonSchemaProperty.maxLength = mask.maxLength;
              }

              return hasFormat;
            });
          }
        }
        let mask = findInArray(masksArray, (item) => {

          if (!item.matcher && item.format) {
            item.matcher = this.doubleAgent().getFormat(item.format);
          }
          if (!item.matcher) {
            throw new Error(`[DoubleAgentValidator] A "ui.matcher" or "ui.format" is required to jsonSchema property "${this.formControlName().name}"`);
          }

          const found: boolean = new RegExp(item.matcher).test(input.value);

          if (found && !jsonProperty.format &&
            (this.origiMaxLength > 0 && this.origiMaxLength != this.formControl.jsonSchemaProperty.maxLength)) {
            this.formControl.jsonSchemaProperty.maxLength = this.origiMaxLength;
          }

          return found;
        });
        return mask ? mask['value'] : null;
      } else {
        return jsonProperty['ui'] ? jsonProperty['ui']['mask'] : null;
      }
    }
    return null;
  }

  @HostListener('input', ['$event.target']) onInput(input: HTMLInputElement) {
    // Write back to model
    if (input && input.value) {

      let mask = this.getUiMask(input);

      if (mask) {
        if (mask.match(/^currency/)) {
          let precision = 2;
          let showSignal = false;
          const matchCurrencyWithPrecision = mask.match(/currency\:(\d+)$/);
          if (matchCurrencyWithPrecision) {
            precision = Number.parseInt(matchCurrencyWithPrecision[1])
          }

          const matchCurrencyWithShowSignal = mask.match(/currency:showSignal$/);
          if (matchCurrencyWithShowSignal) {
            showSignal = true;
          }

          VMasker(input).maskMoney({
            precision: precision,
            showSignal: showSignal
          });
        } else {
          VMasker(input).maskPattern(mask);
          if (this.formControl && this.formControl.jsonSchemaProperty.maxLength) {
            this.renderer.setElementAttribute(input, 'maxlength', this.formControl.jsonSchemaProperty.maxLength.toString());
          }
        }
        setTimeout(() => {
          input.setSelectionRange(input.value.length, input.value.length);
        }, 0);
      }
      this.writeValue(input.value);
    }
  }

  writeValue(value: string) {
    this.formControlName().control.setValue(value);
  }
}
