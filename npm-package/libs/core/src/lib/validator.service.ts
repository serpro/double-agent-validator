import { ValidationResult } from './models/validation-result';
import * as ajvNsAndConstructor from 'ajv';
import { JsonSchema } from './models/schema/json-schema';
import { Injectable } from '@angular/core';

import { ReplaySubject } from 'rxjs';
import { Validator } from './models/validator';

@Injectable()
export class DoubleAgentValidator implements Validator {
  private scriptContext: Window;

  isReady: ReplaySubject<void> = new ReplaySubject<void>(1);

  /**
   * Creates an instance of DoubleAgentValidator.
   */
  constructor() {}

  get ajv(): ajvNsAndConstructor.Ajv {
    return this.scriptContext['ajv'];
  }

  validate(
    schemaName: string,
    data: any,
    options: { removeEmptyStrings: boolean; parseResultFn?: Function } = {
      removeEmptyStrings: true
    }
  ): ValidationResult {
    const result: ValidationResult = this.scriptContext['DoubleAgent'][
      'JsonSchemaValidator'
    ].validate(schemaName, data, options);
    return result;
  }

  getSchema(schemaName: string): JsonSchema {
    return <JsonSchema>this.scriptContext['DoubleAgent'][
      'JsonSchemaValidator'
    ].getSchemaObject(schemaName);
  }

  getKeywords(schema: JsonSchema): string[] {
    return this.scriptContext['DoubleAgent']['JsonSchemaValidator'].getKeywords(
      schema
    );
  }

  getFormats(): {} {
    return this.scriptContext['DoubleAgent'][
      'JsonSchemaValidator'
    ].getFormats();
  }

  getFormat(format: string): {} {
    return this.scriptContext['DoubleAgent']['JsonSchemaValidator'].getFormat(
      format
    );
  }

  get schemasNames(): string[] {
    return this.scriptContext['DoubleAgent'][
      'JsonSchemaValidator'
    ].getSchemas();
  }
}
