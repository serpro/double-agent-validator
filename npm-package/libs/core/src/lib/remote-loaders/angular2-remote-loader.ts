import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RemoteLoader } from '../models/remote-loader';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class Angular2RemoteLoader implements RemoteLoader {
  constructor(private http: HttpClient) {}

  getScript(url: string) {
    return this.http.get(url, { responseType: 'json' }).toPromise() as Promise<
      string
    >;
  }
}
