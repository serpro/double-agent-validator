import {
  Injector,
  ModuleWithProviders,
  InjectionToken,
  NgModule,
  APP_INITIALIZER
} from '@angular/core';
import { DoubleAgentValidator } from './validator.service';
import { DoubleAgentValidatorNg2Factory } from './ng2-factory.service';
import {
  DoubleAgentFormGroupBuilder
} from './form/form-group-builder.service';

import { DoubleAgentFormControlValidatorBuilder } from './form/form-control-validator-builder.service';
import { RemoteLoader } from './models/remote-loader';
import { Angular2RemoteLoader } from './remote-loaders/angular2-remote-loader';
import { DoubleAgentValueVerifier } from './value-verifier.service';
import { DoubleAgentValidatorDirectivesModule } from './directives.module';

export const DOUBLE_AGENT_VALIDATOR_SCHEMA_URL = new InjectionToken(
  'DoubleAgentValidator.SCHEMA_URL'
);
export const DOUBLE_AGENT_VALIDATOR_SCHEMA_WITH_DEPENDENCIES = new InjectionToken(
  'DoubleAgentValidator.SCHEMA_WITH_DEPENDENCIES'
);

let injector, doubleAgentFactory, urlSchema;

export function FactoryBuilder() {
  return DoubleAgentValidatorNg2Factory.factoryFn(
    injector,
    doubleAgentFactory,
    urlSchema
  );
}

export function DoubleAgentValidatorInitializerFactory(
  injectorP: Injector,
  doubleAgentFactoryP: DoubleAgentValidatorNg2Factory,
  urlP: string
) {
  const factoryBuilder = FactoryBuilder;
  injector = injectorP;
  doubleAgentFactory = doubleAgentFactoryP;
  urlSchema = urlP;
  return factoryBuilder;
}

export const DoubleAgentValidatorProviders = [
  DoubleAgentValidator,
  DoubleAgentValidatorNg2Factory,
  DoubleAgentFormGroupBuilder,
  DoubleAgentValueVerifier,
  DoubleAgentFormControlValidatorBuilder,
  {
    provide: RemoteLoader,
    useClass: Angular2RemoteLoader
  },
  {
    provide: APP_INITIALIZER,
    useFactory: DoubleAgentValidatorInitializerFactory,
    deps: [
      Injector,
      DoubleAgentValidatorNg2Factory,
      DOUBLE_AGENT_VALIDATOR_SCHEMA_URL
    ],
    multi: true
  }
];

@NgModule({
  providers: [DoubleAgentValidatorProviders],
  imports: [DoubleAgentValidatorDirectivesModule]
})
export class DoubleAgentValidatorModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DoubleAgentValidatorModule,
      providers: DoubleAgentValidatorProviders
    };
  }
}
