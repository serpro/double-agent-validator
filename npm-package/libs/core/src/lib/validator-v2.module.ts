import { NgModule, ModuleWithProviders } from '@angular/core';
import { DoubleAgentValidatorV2 } from './validator-v2.service';
import { DoubleAgentValidatorConfig } from './models/validator-config';
import { DoubleAgentValidator } from './validator.service';
import { DoubleAgentFormGroupBuilder } from './form/form-group-builder.service';
import { DoubleAgentValueVerifier } from './value-verifier.service';
import { DoubleAgentFormControlValidatorBuilder } from './form/form-control-validator-builder.service';
import { DoubleAgentValidatorDirectivesModule } from './directives.module';
import { ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';

export function factoryForDoubleAgentFormGroupBuilder(dav,daf,fb) {
  return new DoubleAgentFormGroupBuilder(dav, daf, fb);
}
export { ValidationFn } from './models/validation-function';
export const DOUBLE_AGENT_VALIDATOR_PROVIDERS = [
  {
    provide: DoubleAgentValidator,
    useClass: DoubleAgentValidatorV2
  },
  {
    provide: DoubleAgentFormGroupBuilder,
    useFactory: factoryForDoubleAgentFormGroupBuilder,
    deps: [DoubleAgentValidator, DoubleAgentFormControlValidatorBuilder, FormBuilder],
  },
  DoubleAgentValueVerifier,
  DoubleAgentFormControlValidatorBuilder
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    DoubleAgentValidatorDirectivesModule
  ],
  exports: [DoubleAgentValidatorDirectivesModule]
})
export class DoubleAgentValidatorV2Module {
  static forRoot(config: DoubleAgentValidatorConfig): ModuleWithProviders<DoubleAgentValidatorV2Module> {
    return {
      ngModule: DoubleAgentValidatorV2Module,
      providers: [
        {
          provide: DoubleAgentValidatorConfig,
          useValue: config
        },
        ...DOUBLE_AGENT_VALIDATOR_PROVIDERS
      ]
    };
  }
}
