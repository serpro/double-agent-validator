import { ValidationResult } from './validation-result';

export interface ValidationFn {
  (schemaName: string, data: any): ValidationResult;
}
