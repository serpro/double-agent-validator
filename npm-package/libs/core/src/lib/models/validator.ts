import { ValidationResult } from './validation-result';
import { Ajv } from 'ajv';
import { JsonSchema } from './schema/json-schema';

export interface Validator {
   schemasNames: string[];
   ajv: Ajv;
   validate(schemaName: string, data: any): ValidationResult | Promise<any>;
   getSchema(schemaName: string): JsonSchema;
   getKeywords(schema: JsonSchema): string[];
   getFormats(): {};
   getFormat(format: string): {};
}
