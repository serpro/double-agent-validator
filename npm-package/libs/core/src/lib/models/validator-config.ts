import { Ajv } from 'ajv';
import { ValidationFn } from './validation-function';
import { Injectable } from '@angular/core';

Injectable()
export class DoubleAgentValidatorConfig {
  public ajv: any;
  public  validationFn: ValidationFn;
  constructor() {
  }

}
