import { JsonSchemaProperty } from '../schema/json-schema-property';
export interface PropertiesCollection {
    [property: string]: JsonSchemaProperty;
}
