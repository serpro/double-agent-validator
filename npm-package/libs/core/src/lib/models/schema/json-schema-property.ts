import { JsonSchema } from './json-schema';
import { PropertiesCollection } from './properties-collection';
export interface JsonSchemaProperty {
  type: string;
  format?: string;
  pattern?: RegExp;
  ui?: any;
  maxLength?: number;
  minLength?: number;
  items?: JsonSchema;
  properties?: PropertiesCollection;
  skip?: boolean;
  group?: boolean;
  groupArray?: boolean;
}
