import { DoubleAgentAbstractControl } from './abstract-control';
import { DoubleAgentFormGroup } from './form-group';
import { AbstractControl, FormControl, FormArray } from '@angular/forms';


export interface DoubleAgentFormControl extends FormControl, DoubleAgentAbstractControl {}

export interface DoubleAgentFormArray extends FormArray, DoubleAgentAbstractControl {
  newGroup: (controls?: { [key: string]: AbstractControl }) => DoubleAgentFormGroup;
  addGroup: (controls?: { [key: string]: AbstractControl }) => DoubleAgentFormGroup;
}


export type DoubleAgentControl = DoubleAgentFormControl | DoubleAgentFormGroup | DoubleAgentFormArray;
