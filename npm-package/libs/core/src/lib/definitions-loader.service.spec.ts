import { ValidatorDefinitionsLoader } from './definitions-loader.service';
import { DoubleAgentValidator } from './validator.service';
// import * as fs from 'fs';
import * as jsdomNS from 'jsdom';

// import { expect } from 'chai';
import { InTestRawLoader } from './remote-loaders/in-test-raw-loader';
import { getDoubleAgentMockedScriptContent } from './directives/mock';

describe('ValidatorDefinitionsLoader', () => {
  const scriptContent = getDoubleAgentMockedScriptContent();
  let doubleAgentValidator = new DoubleAgentValidator();
  let loader: ValidatorDefinitionsLoader;
  let remoteLoader;

  let window: Window;

  beforeEach(() => {
    remoteLoader = new InTestRawLoader(scriptContent, doubleAgentValidator);
    loader = new ValidatorDefinitionsLoader(remoteLoader);
    const objDOM = new jsdomNS.JSDOM(
      '<html><body>Página de Teste</body></html>',
      { runScripts: 'dangerously', url: 'http://localhost' }
    );
    window = objDOM.window;
  });

  it('loads script from remote module', done => {
    loader.load(window, scriptContent).then(() => {
      doubleAgentValidator = new DoubleAgentValidator();
      doubleAgentValidator['scriptContext'] = window;
      const result = doubleAgentValidator.validate('contribuinte-v1', {
        id: 1,
        nome: 'John',
        ni: '00.000.000/0001-91',
        nacionalidade: 'brasileiro'
      });
      expect(result.hasErrors).toBeFalsy();
      done();
    });
  });
});
