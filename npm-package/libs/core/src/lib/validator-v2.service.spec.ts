import { Component } from '@angular/core';
import { FormGroup, ReactiveFormsModule, FormControl } from '@angular/forms';
import { TestBed, getTestBed, ComponentFixture } from '@angular/core/testing';

import { DoubleAgentValidatorV2Module } from './validator-v2.module';
import { DoubleAgentValidator } from './validator.service';
import { DoubleAgentFormGroupBuilder } from './form/form-group-builder.service';

import { ValidationResult } from './models/validation-result';
import { DoubleAgentFormGroup } from './form/form-group';

import { ajv, validationFn } from '../testing/mock-data/fixture-ajv-bundled';
import { triggerInput } from './directives/test-helpers';

let bundledDA: DoubleAgentValidator;

describe('DoubleAgentValidatorV2', () => {
  @Component({
    selector: 'doubleagent-not-used',
    template: `<form [formGroup]="daForm">
        <input type="text" formControlName="name"  />
        <input type="text" formControlName="investments"  />
      </form>
      `
  })
  class WithInputMaskComponent {
    public daForm: FormGroup;
    constructor(fb: DoubleAgentFormGroupBuilder) {
      this.daForm = fb.build('person', { createValidators: false });
    }

    get fieldName(): FormControl {
      return <FormControl>this.daForm.get('name');
    }
  }

  beforeEach(() => {
    return getTestBed().configureTestingModule({
      declarations: [WithInputMaskComponent],
      imports: [
        ReactiveFormsModule,
        DoubleAgentValidatorV2Module.forRoot({
          ajv: ajv,
          validationFn: validationFn
        })
      ]
    });
  });

  beforeEach(() => {
    bundledDA = getTestBed().get(DoubleAgentValidator);
  });

  describe('DoubleAgentValidator in DoubleAgentValidatorV2Module', () => {
    it('validates', () => {
      const result = <ValidationResult>bundledDA.validate('person', {});
      expect(result.hasErrors).toBeTruthy();
    });
  });

  describe('DoubleAgentFormGroupBuilder in DoubleAgentValidatorV2Module', () => {
    let formBuilder: DoubleAgentFormGroupBuilder;
    let formGroup: DoubleAgentFormGroup;
    beforeEach(() => {
      formBuilder = getTestBed().get(DoubleAgentFormGroupBuilder);
    });
    it('builds form without validators', () => {
      formGroup = <DoubleAgentFormGroup>formBuilder.build('person', {
        createValidators: false
      });
      expect(formGroup.get('name')).toBeDefined();
    });
    it('build form with validators', () => {
      formGroup = <DoubleAgentFormGroup>formBuilder.build('person');
      expect(formGroup.get('name')).toBeDefined();
    });
  });

  describe('InputMaskDirective in DoubleAgentValidatorV2Module', () => {
    let fixture: ComponentFixture<WithInputMaskComponent>;

    beforeEach(() => {
      fixture = TestBed.createComponent(WithInputMaskComponent);
    });

    it('masks the text input filtering chars', () => {
      const field = triggerInput(fixture, {
        fieldName: 'investments',
        value: 'AA'
      });
      expect(field.input.value).toEqual('');
    });

    it('masks the text input filtering chars', () => {
      const field = triggerInput(fixture, {
        fieldName: 'investments',
        value: '3240'
      });
      expect(field.input.value).toEqual('32,40');
    });
  });
});
