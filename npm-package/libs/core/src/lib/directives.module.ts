import { NgModule } from '@angular/core';
import { InputHtml5AttributesDirective } from './directives/input-html5-attributes.directive';
import { InputMaskDirective } from './directives/input-mask.directive';

@NgModule({
  declarations: [InputHtml5AttributesDirective, InputMaskDirective],
  exports: [InputHtml5AttributesDirective, InputMaskDirective],
  entryComponents: []
})
export class DoubleAgentValidatorDirectivesModule {}
