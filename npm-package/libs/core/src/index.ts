/*
 * Public API Surface of double-agent-validator
 */

import './lib/polyfills';
export { DoubleAgentFormGroup } from './lib/form/form-group';
export { DoubleAgentFormControl, DoubleAgentFormArray, DoubleAgentControl } from './lib/form/form-control';
export {
    DoubleAgentFormControlValidatorBuilder
} from './lib/form/form-control-validator-builder.service';
export { DoubleAgentValidator } from './lib/validator.service';

export { DoubleAgentValidatorConfig } from './lib/models/validator-config';
export { ValidatorDefinitionsLoader } from './lib/definitions-loader.service';
export {
    DoubleAgentValidatorModule,
    DOUBLE_AGENT_VALIDATOR_SCHEMA_URL
} from './lib/validator.module';

export {
    DoubleAgentFormGroupBuilder,
    FormGroupStates,
    FormControlState
} from './lib/form/form-group-builder.service';
export { ValidationResult } from './lib/models/validation-result';
export { RemoteLoader } from './lib/models/remote-loader';
export {
    JsonSchema,
} from './lib/models/schema/json-schema';

export { PropertiesCollection } from './lib/models/schema/properties-collection';
export { JsonSchemaProperty, } from './lib/models/schema/json-schema-property';
export { DoubleAgentValueVerifier } from './lib/value-verifier.service';
export { Angular2RemoteLoader } from './lib/remote-loaders/angular2-remote-loader';
export {
    NodeRemoteLoader
} from './lib/remote-loaders/node-remote-loader';
export {
    InTestRawLoader,
} from './lib/remote-loaders/in-test-raw-loader';
export { DoubleAgentValidatorNg2Factory } from './lib/ng2-factory.service';
export {
    InputHtml5AttributesDirective,
} from './lib/directives/input-html5-attributes.directive';
export {
  InputMaskDirective
} from './lib/directives/input-mask.directive';
export { getMockForMask, triggerInput } from './lib/directives/test-helpers';

/** Double Agent Validator V2 - Bundled AJV - to be used with bundled generated using @pratico/json-schema-store template */
export { DoubleAgentValidatorV2 } from './lib/validator-v2.service';

