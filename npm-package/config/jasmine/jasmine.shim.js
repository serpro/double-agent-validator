
require('core-js/es6/reflect');
require('core-js/es7/reflect');

require('zone.js');
require('zone.js/dist/proxy.js');
require('zone.js/dist/sync-test');
require('zone.js/dist/async-test');
require('zone.js/dist/fake-async-test');
require('zone.js/dist/jasmine-patch');

const {
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting
} = require('@angular/platform-browser-dynamic/testing');
const { TestBed, getTestBed } = require('@angular/core/testing');
getTestBed().initTestEnvironment(
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting()
);

var fs = require('fs');

require.extensions['.json'] = function (module, filename) {
  module.exports = JSON.parse(fs.readFileSync(filename, 'utf8'));
};

require.extensions['.html'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};

require.extensions['.scss'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};

require.extensions['.css'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};
