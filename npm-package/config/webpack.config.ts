require('ts-node/register');
import * as yargs from 'yargs';


import { getBuildConfig } from './webpack.build.config';


let argv = yargs.argv;

let config = {};

// get webpack config for build the project
config = getBuildConfig('double-agent-validator.umd.js', 'DoubleAgentValidator');


export default config;
